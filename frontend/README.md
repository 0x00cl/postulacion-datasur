# Frontend

El frontend solo tiene la ruta, `/` el cual muestra una grilla con tarjetas con la información de los distintos trabajos

![Inicio de la pagina web](screenshots/web-jobs.png)

En la barra de arriba de la pagina web donde dice "Filtra por tecnologias" se puede escribir algún lenguaje de programación en **minuscula** y filtrará la tarjetas que muestra en base a la tecnologia.

![Trabajos filtrados](screenshots/web-jobs-filtered.png)

## Proceso, decisiones y dificultades

Nunca habia ocupado algún web framework para el desarrollo de paginas web como react, angular, vue, svelte, etc. Por lo que decidí utilizar Next.js (react) ya que era lo que se sugería para utilizar en el documento.

Al nunca haber ocupado un web framework fue complejo entender como se estructuraban y las reglas que tiene. Por ejemplo, por lo menos con Next.js, al intentar obtener datos de la API e intentar integrar el filtro de trabajos aparecian errores como que no se podian utilizar componentes que eran para cliente en un componente de servidor, y apartir de eso fue dificil entender que se debia definir un componente como cliente y luego integrarlo en otro componente para que funcionase. 

## Requerimientos

- nodejs (Probado con 21.6.0)
- npm (Probado con 10.3.0)

## Instalación

Se deben descargar las dependencias

```
$ npm install
```

## Ejecución

Ambiente desarrollo

```
$ npm run dev
```

Debiese quedar disponible en el puerto 3000 (por defecto)