
import axios from 'axios'
import Search from '@/app/lib/search';
import { Job } from './lib/definitions';
import { Suspense } from 'react';
import { fetchFilteredInvoices } from '@/app/lib/jobgrid'
import Table from '@/app/lib/jobgrid'

export default async function Home({
  searchParams,
}: {
  searchParams?: {
    technologies?: string;
  };
}) {
  const query = searchParams?.technologies || '';
  return (
    <main>
      <div className="flex w-full items-center justify-between">
        <h1 className={`text-2xl`}>Trabajos</h1>
      </div>
      <div className="mt-4 flex items-center justify-between gap-2 md:mt-8">
        <Search placeholder="Filtra por tecnologias" />
      </div>
      <Suspense key={query}>
        <Table query={query} />
      </Suspense>
    </main>
  );
}