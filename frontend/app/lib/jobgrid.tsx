import axios from 'axios'
import { Job } from './definitions';

export async function fetchFilteredInvoices({query}: {query: string}) {
    const res = await axios.get('http://localhost:5000/jobs?technologies='+query)
    return res.data  
  }
  

export default async function InvoicesTable({
  query,
}: {
  query: string;
}) {
  const jobs = await fetchFilteredInvoices({query});

  return (
    <div className='w-11/12 m-auto md:grid mt-10 gap-10 md:grid-cols-4 flex flex-col'>
        {jobs.map((job: Job) => (
          <div key={job.id} className='bg-white shadow-lg rounded-md overflow-hidden h-[14rem] cursor-pointer'>
          <span className='px-3 py-2 block font-bold text-xl'>{job.name}</span>
          <span className='px-3 py-2 block'><span className='font-bold'>Empresa:</span> <span className='font-light'>{job.company}</span></span>
          <span className='px-3 py-2 block'><span className='font-bold'>Tecnologia:</span> <span className='font-light'>{job.technologies}</span></span>
          </div>
        ))}
      </div>
  );
}