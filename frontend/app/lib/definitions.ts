export type Job = {
    technologies: string;
    id: string,
    name: string,
    company: string,
}