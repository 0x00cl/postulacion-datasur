# Backend

El backend solo expone una ruta, `/jobs` el cual retorna una lista no paginada de los trabajos que están almacenados en la base de datos, en este caso por ser algo simple y rapido se utilizó SQLite3.

Esta ruta permite recibir argumentos o queries con el nombre `technologies`, que permite filtrar los trabajos por technologia, por ejemplo para filtrar los trabajos con tecnologia Python se debe realizar una consulta a `/jobs?technologies=python`.

## Proceso, decisiones y dificultades

Con Python ya tenia harta experiencia trabajando con el. También tenia algo de experiencia con Flask y SQLAlchemy por lo que no hubieron mayores complejidades.

Decidí optar por utilizar SQLite3 simplemente porque no requiere tener una base de datos corriendo haciendo más simple el desarrollo y su ejecución.

Ahora, si hay un detalle que podria mejorar pero que por falta de tiempo prefiero no desarrollar y es que entregue los resultados paginados para evitar que cargue muchos datos de una (si es que eventualmente hubiesen muchas solicitudes sobrecargando el servidor). Flask-SQLAlchemy tiene una función para paginar [`paginate()`](https://flask-sqlalchemy.palletsprojects.com/en/3.1.x/api/#flask_sqlalchemy.SQLAlchemy.paginate) los resultados.

## Requerimientos

- Python ^3.11
- [Poetry](https://python-poetry.org/)

### Opcional (recomendado)

- [Poethepoet](https://poethepoet.natn.io/)

## Instalación

1. Teniendo poetry instalado, se pueden instalar las dependencias con este comando.

    ```
    $ poetry install
    ```

2. Luego, hay que crear las tablas y llenar la base de datos, para esto si se tiene poe instalado se puede ejecutar el siguiente comando:

    ```
    $ poe init-db-full
    ```

    Si no se instaló Poethepoet se puede ejecutar de la siguiente forma:

    ```
    $ poetry run flask --app postulacion_datasur init-db-tables && poetry run flask --app postulacion_datasur init-db-data
    ```

## Ejecución

Para su ejecución en un ambiente de desarrollo sería:

```
$ poe serve
```

Nuevamente, si no se instaló poethepoet se puede ejecutar de la siguiente forma:

```
$ poetry run flask --app postulacion_datasur run --debug --host=0.0.0.0
```

Y quedaria disponible el backend en el puerto 5000 (por defecto)