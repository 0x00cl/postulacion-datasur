from pathlib import Path

from flask import Flask, jsonify, request

from . import db

app = Flask(__name__, instance_relative_config=True)
app.config.from_pyfile("config.py", silent=True)

# app.instance_path: Creación de la carpeta `instance`
Path(app.instance_path).mkdir(parents=True, exist_ok=True)

db.init_app(app)


@app.route("/jobs")
def jobs():
    technology = request.args.get("technologies", "")
    remote_jobs_query = db.sa.select(db.Jobs)
    if technology != "":
        remote_jobs_query = remote_jobs_query.where(db.Jobs.technologies == technology)
    remote_jobs = db.sa.session.execute(remote_jobs_query).scalars().all()
    return jsonify([job.as_dict() for job in remote_jobs])
