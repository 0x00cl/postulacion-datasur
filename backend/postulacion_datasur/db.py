import decimal

import click
from flask.cli import with_appcontext
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column

from .scripts.jobs_data import load_jobs_data


class Base(DeclarativeBase):
    pass


sa = SQLAlchemy(model_class=Base)


def init_app(app):
    app.cli.add_command(init_db_tables)
    app.cli.add_command(init_db_data)
    sa.init_app(app)


class Jobs(sa.Model):
    id: Mapped[int] = mapped_column(sa.Integer, primary_key=True)
    isPartner: Mapped[bool] = mapped_column(sa.Boolean, nullable=True)
    isFullRemote: Mapped[bool] = mapped_column(sa.Boolean, nullable=True)
    isPaused: Mapped[bool] = mapped_column(sa.Boolean, nullable=True)
    longitude: Mapped[decimal.Decimal] = mapped_column(sa.Numeric)
    latitude: Mapped[decimal.Decimal] = mapped_column(sa.Numeric)
    cityCategory: Mapped[str] = mapped_column(sa.String, nullable=True)
    stateCategory: Mapped[str] = mapped_column(sa.String, nullable=True)
    tier: Mapped[str] = mapped_column(sa.String, nullable=True)
    activeFrom: Mapped[str] = mapped_column(sa.String, nullable=True)
    companyWebsiteLink: Mapped[str] = mapped_column(sa.String, nullable=True)
    candidateContactWay: Mapped[str] = mapped_column(sa.String, nullable=True)
    redirectJobUrl: Mapped[str] = mapped_column(sa.String, nullable=True)
    company: Mapped[str] = mapped_column(sa.String, nullable=True)
    address: Mapped[str] = mapped_column(sa.String, nullable=True)
    actualCity: Mapped[str] = mapped_column(sa.String, nullable=True)
    postalCode: Mapped[str] = mapped_column(sa.String, nullable=True)
    companyType: Mapped[str] = mapped_column(sa.String, nullable=True)
    companySize: Mapped[str] = mapped_column(sa.String, nullable=True)
    hasVisaSponsorship: Mapped[str] = mapped_column(sa.String, nullable=True)
    language: Mapped[str] = mapped_column(sa.String, nullable=True)
    perkKeys: Mapped[str] = mapped_column(sa.String, nullable=True)
    offerStockOrBonus: Mapped[str] = mapped_column(sa.String, nullable=True)
    name: Mapped[str] = mapped_column(sa.String, nullable=True)
    jobType: Mapped[str] = mapped_column(sa.String, nullable=True)
    jobUrl: Mapped[str] = mapped_column(sa.String, nullable=True)
    expLevel: Mapped[str] = mapped_column(sa.String, nullable=True)
    annualSalaryFrom: Mapped[str] = mapped_column(sa.String, nullable=True)
    annualSalaryTo: Mapped[str] = mapped_column(sa.String, nullable=True)
    techCategory: Mapped[str] = mapped_column(sa.String, nullable=True)
    technologies: Mapped[str] = mapped_column(sa.String, nullable=True)
    filterTags: Mapped[str] = mapped_column(sa.String, nullable=True)

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


@click.command("init-db-tables")
@with_appcontext
def init_db_tables():
    sa.drop_all()
    click.echo("Tablas actuales eliminadas.")
    sa.create_all()
    click.echo("Tablas creadas.")


@click.command("init-db-data")
@with_appcontext
def init_db_data():
    click.echo("Obteniendo y cargando datos de trabajos de la API.")
    jobs_data = load_jobs_data()
    for job in jobs_data:
        job.pop("logoImg", None)
        job.pop("_id", None)
        job.pop("companyId", None)
        job.pop("cpc", None)
        job.pop("disableMethodology", None)
        job.pop("expiresOn", None)
        job["technologies"] = (
            job["technologies"][0].lower() if job["technologies"] else ""
        )
        job["filterTags"] = job["filterTags"][0] if job["filterTags"] else ""
        job["perkKeys"] = job["perkKeys"][0] if job["perkKeys"] else ""
        sa.session.add(Jobs(**job))
    sa.session.commit()
    click.echo("Datos cargados en la base de datos local.")
