#!/usr/bin/env python
import json
from pathlib import Path, PurePath

import requests


def load_jobs_data():
    try:
        resp = requests.get("https://devitjobs.com/api/jobsLight", timeout=5)
        resp.raise_for_status()
    except requests.exceptions.RequestException as err:
        print(f"Ocurrio un error al obtener los datos: {err}")
        script_dir = PurePath(__file__).parent
        with Path(script_dir).joinpath("jobsLight.json").open() as f:
            resp = json.load(f)
    else:
        resp = resp.json()
    return resp


if __name__ == "__main__":
    print(load_jobs_data())
