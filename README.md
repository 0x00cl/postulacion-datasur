# Postulación datasur

Este proyecto es para la postulación de desarrollador para datasur.

El proyecto está compuesto de 2 directorios, el frontend y el backend.
Cada uno de estos proyectos tiene su propio archivo README.md donde se detallan los requerimientos, como se debe ejecutar y las decisiones que se tomaron.

## Detalles del proyecto

Este proyecto es una simple pagina web, que muestra los trabajos disponibles obtenidos a partir de la API https://devitjobs.com/api/jobsLight. Este muestra el nombre del trabajo, de la compañia y la tecnologia que utiliza. Los trabajos se pueden filtrar por tecnologia como por ejemplo `python` o `dart` escribiendo en la barra de filtros.

### Backend

El backend está hecho en Python con [Flask](https://flask.palletsprojects.com/en/3.0.x/) y [SQLAlchemy](https://docs.sqlalchemy.org/en/20/) para el acceso a la base de datos.

### Frontend

El frontend está hecho con [Next.js](https://nextjs.org/) un framework de [react](https://react.dev/).

(En el README.md del frontend incluye capturas de pantallas)
